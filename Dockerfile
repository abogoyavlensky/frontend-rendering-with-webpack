FROM node:alpine

COPY . /app
WORKDIR /app

RUN npm install && npm run postinstall

EXPOSE 9009

CMD npm run watch
